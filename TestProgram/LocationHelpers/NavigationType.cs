namespace RhinoBot.LocationHelpers
{
	public enum NavigationType
	{
		None,
		Coordinates,
		CurrentTarget,
		SpawnId,
		SpawnSearch,
		TargetOfGroupMemberName,
		WaypointName
	}
}
