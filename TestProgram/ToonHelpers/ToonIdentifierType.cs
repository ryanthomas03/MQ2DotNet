namespace RhinoBot.ToonHelpers
{
	public enum ToonIdentifierType
	{
		None,
		CurrentGroup,
		CurrentToon,
		GroupMemberIndex,
		GroupName,
		ToonName,
		ToonId
	}
}
